if not exist .\main.py (
    echo "Please change directory!"
    exit 1
) 

if not exist .\venv\ (
    python -m venv venv
    .\venv\Scripts\activate
    pip install -r requirements.txt
) else (
    venv\bin\activate
)

python .\main.py
deactivate