import configparser
import os.path

import PySimpleGUI as sg

from lib import mainMenu, logfile_handler

# ---------------------------------
# VARIABLES
# ---------------------------------
scriptPath = os.path.abspath(os.path.dirname(__file__))+'/'

# strings to get the specific values from JSON/Dictionary
jsonIdentifierPW = 'password'
jsonIdentifierWebsite = 'website'

# read from main.conf
config = configparser.ConfigParser()
config.read(r'main.conf')
jsonFile = scriptPath + config.get('RemoteMailmanAdmin', 'connectionFile')
subsCSV = scriptPath + config.get('RemoteMailmanAdmin', 'allSubscribersOutputCSV')
sessionLogging = bool(scriptPath + config.get('RemoteMailmanAdmin', 'sessionLogging'))
if config.has_option('RemoteMailmanAdmin', 'defaultMailingLists'):
    defaultMailingLists = config.get('RemoteMailmanAdmin', 'defaultMailingLists').split(",")
else:
    defaultMailingLists = ''

log = logfile_handler.Log(scriptPath, enabled=sessionLogging)

# ---------------------------------
# GET ENCRYPTED DATA
# ---------------------------------
# read JSON Data from encrypted JSONFile
# passwordreader will decrypt, read and encrypt jsonFile

# json.dumps(jsonData)

if os.path.exists(jsonFile):
    sg.theme('Reddit')
    mainMenu = mainMenu.MainMenu(jsonFile, defaultMailingLists, subsCSV, log=log)
    mainMenu.run()
else:
    sg.popup_error('Error: missing file: '+jsonFile+'\n' +
                   'Tip: copy sample file and rename it to '+jsonFile+'\n' +
                   'you can later at mailinglists with "Manage Mailinglists".')
    log.add('ERR', 'missing file '+jsonFile)

