import datetime
import os


class Log:

    loggingFile = ''
    enabled = False
    stdout = True

    def __init__(self, mainPath='', enabled=False, stdout=True):
        if enabled:
            timestamp = datetime.date.today().strftime('%Y%m%d')
            loggingFilePath = mainPath + 'logging/'
            self.loggingFile = loggingFilePath + timestamp + '_'
            counter = 1
            for root, directories, files in os.walk(loggingFilePath):
                for file in files:
                    if file.startswith(timestamp):
                        counter = counter + 1
            self.enabled = enabled
            self.stdout = stdout
            self.loggingFile += str(counter) + '.log'
            print(self.loggingFile)
            logFile = open(self.loggingFile, 'w')
            logFile.write(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + ' NEW SESSION \n')

    def add(self, cat, entry):
        if self.enabled:
            file = open(self.loggingFile, 'a')
            file.write(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + ' ' + cat + ': ' + entry + '\n')
        if self.stdout:
            print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + ' ' + cat + ': ' + entry)
