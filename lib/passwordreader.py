import base64
import json
import os.path
import sys

from cryptography.fernet import Fernet
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC

# password = bytes(stdiomask.getpass(prompt='\nEnter password - ', mask='*'), 'utf-8')
salt = bytes('S@lrV3lU5-v!'.encode('utf8'))
# listFile = "list.json"

# HELP Source: https://medium.com/@abhishake21/encrypt-and-decrypt-files-and-folder-with-a-password-and-salt-of-your-choice-using-python-7101840b2753


def create_key(pw):
    # pw = bytes(stdiomask.getpass(prompt='\n'+msg, mask='*'), 'utf-8')
    if not os.path.exists('key.sec'):
        kdf = PBKDF2HMAC(
            algorithm=hashes.SHA256(),
            length=32,
            salt=salt,
            iterations=100000,
            backend=default_backend())

    key = base64.urlsafe_b64encode(kdf.derive(pw))
    return key


def encrypt_file(file, f):
    with open(file, 'rb') as original_file:
        original = original_file.read()

    encrypted = f.encrypt(original)

    with open(file, 'wb') as encrypted_file:
        encrypted_file.write(encrypted)
    encrypted_file.close()


def decrypt_file(file, f):
    with open(file, 'rb') as original_file:
        original = original_file.read()

    try:
        decrypted = f.decrypt(original)
    except:
        print('ERR: password is incorrect!')
        sys.exit()

    with open(file, 'wb') as decrypted_file:
        decrypted_file.write(decrypted)
    decrypted_file.close()


def check_if_file_is_encrypted(file):
    isEncrypted = 0

    if not os.path.exists(file):
        sys.exit('ERR: file "'+file+'" does not exist!')

    with open(file, 'r') as openfile:
        for count, line in enumerate(openfile):
            pass
    openfile.close()

    if count == 0:
        isEncrypted = 1

    return bool(isEncrypted)


def get_json(file, pw):  # <-- THiS COMMAND IS USED OUTSIDE!
    # f = Fernet(create_key('Enter password:'))  # <-- password will be entered here
    f = Fernet(create_key(pw))
    if check_if_file_is_encrypted(file):
        decrypt_file(file, f)

    with open(file, 'r') as openfile:
        data = json.load(openfile)
        # data = openfile.read()
    openfile.close()

    encrypt_file(file, f)
    return data


def reset_password_encryption(file, old_pw, new_pw):
    f = Fernet(create_key(old_pw))
    try:
        decrypt_file(file, f)
    except:
        return 'ERR: wrong Password!'
    f = Fernet(create_key(new_pw))
    encrypt_file(file, f)
    return 'Password resetted!'


def save_changes(file, content, pw):
    f = Fernet(create_key(pw))
    with open(file, 'wt') as opened_file:
        opened_file.write(json.dumps(content, indent=4))
    opened_file.close()
    encrypt_file(file, f)

