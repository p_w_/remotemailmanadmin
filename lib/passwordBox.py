import PySimpleGUI as sg


class PasswordBox:
    layout = []
    window = 0

    def __init__(self, title='Enter Password'):
        self.generate_layout()
        self.window = sg.Window(title, self.layout, default_element_size=(40, 1), grab_anywhere=False)

    def generate_layout(self):
        self.layout = [
            [sg.InputText('', key='PASSWORD', password_char='*')],
            [sg.Submit('Submit', key='SUBMIT'), sg.Cancel('Quit', key='QUIT')]
        ]

    def start(self):
        while True:
            event, values = self.window.read()

            if event == sg.WINDOW_CLOSED or event == 'QUIT':
                self.window.close()
                return False

            if event == 'SUBMIT':
                self.window.close()
                return values['PASSWORD']
