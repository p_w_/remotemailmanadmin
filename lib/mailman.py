import re

import urllib3


class Mailman:
    http = urllib3.PoolManager()
    password = {'adminpw': ''}
    tempBanList = ""
    url = ""

    def __init__(self, url, password):
        self.url = url
        self.password = {'adminpw': password}

    def download_html(self, url, payload):
        # This function downloads a given HTML page, identifies encoding and returns an interpretable HTML file
        # 'fields=' are variables on the website, these are normally filled by textboxes
        # with 'fields=' these variables can be addressed - just as 'curl --data' - to address these vars a dict is
        # necessary like '{VAR_ON_WEBSITE: VALUE_FOR_VAR}'
        # to get to sites behind login, it is necessary to fill adminpw var on website for mailman
        response = self.http.request('GET', url, fields=payload)
        encoding = str(response.headers).split('charset=')[1].split("'")[0]
        return response.data.decode(encoding)

    def get_result(self, mailmanHTMLSite):
        # This functions checks the first few lines under body. The results must start with an h5 headline
        info = ''
        add_info = bool(1)
        for line in re.split('\n', mailmanHTMLSite):
            if '<h1>' in line and add_info:
                info += (line.split('>')[1].split('<')[0]) + '\n'
            if '<h2>' in line and add_info:
                info += (line.split('>')[1].split('<')[0]) + '\n'
            if '<h3>' in line and add_info:
                info += (line.split('>')[3].split('<')[0]) + '\n'  # there are more formations
            if '<h4>' in line and add_info:
                info += (line.split('>')[1].split('<')[0]) + '\n'
            if '<h5>' in line and add_info:
                info += (line.split('>')[1].split('<')[0]) + '\n'
            elif '<li>' in line and add_info:
                info += ' ' + (line.split('>')[1].split('<')[0]) + '\n'
            elif '<p>' in line and add_info:
                add_info = bool(0)
        return info

    def get_users(self):
        # the member part of mailman will be downloaded as HTML
        # the line of the where the mail addresses are listed will be separated from HTML code
        # and only the addresses remains
        # all mail address (subscribers to this mailinglist) will be returned in an array
        users = []
        url = self.url + '/members'
        html = self.download_html(url, self.password)
        for line in re.split('\n', html):
            if 'name="user"' in line:
                users.append(line.split('>')[2].split('<')[0])
        return users

    def get_banlist(self):
        # if banlist is wanted then a regex will be set '^.*$' so no one can subscribe by himself
        # to add new subscribers banlist has to be deactivated
        # this programm does this automatically, but to be sure list will be same es before it will be temporaly stored
        url = self.url + '/privacy'
        html = self.download_html(url, self.password)
        counter = 0
        addToList = bool(0)
        banList = []
        for line in re.split('\n', html):
            counter = counter+1
            if 'TEXTAREA NAME=ban_list' in line:
                banList.append(line.split('>')[2])
                addToList = bool(1)
            elif '</TEXTAREA></td>' in line and addToList:
                banList.append(line.split('<')[0])
                addToList = bool(0)
            elif addToList:
                banList.append(line)
        if not '^.*$' in banList:
            banList = ""
        return banList

    def disable_banlist(self):
        # banlist of mailinglist will be cleared
        # this is down by "downloading" HTML site of banlist (privacy) and put an empty banlist in the GET-request
        # downloaded website will not be used it is just used for GET-request with payload
        self.tempBanList = self.get_banlist()  # safe old banlist so it can later be resetted
        url = self.url + '/privacy'
        payload = self.password.copy()
        payload.update({'ban_list': ' '})
        self.download_html(url, payload)
        # print("INFO: disabled banlist")

    def activate_banlist(self):
        # banlist of mailinglist will besetted
        # this is down by "downloading" HTML site of banlist (privacy) and put banlist in the GET-request as fields=
        # payload
        # downloaded website will not be used it is just used for GET-request with payload
        # banlist will be the stored banlist before disabled or a new with simply regex for all
        url = self.url + '/privacy'
        payload = self.password.copy()
        newBanlist = ""
        if '^.*$' in self.tempBanList:
            for entry in self.tempBanList:
                newBanlist += entry + '\n'
        else:
            newBanlist = '^.*$'
        payload.update({'ban_list': newBanlist})
        self.download_html(url, payload)
        # print("INFO: activated banlist")

    def add_members(self, members):
        # new subscribers will be added to mailings
        # this is down by "downloading" HTML site of members and put new addresses in the GET-request as fields=
        # payload
        # downloaded website will not be used it is just used for GET-request with payload
        # more than one mail address can be delivered as array or separated by \n
        url = self.url + '/members/add'
        payload = self.password.copy()
        self.disable_banlist()
        if isinstance(members, list):
            memberList = ""
            for member in members:
                memberList += member + '\n'
        else:
            memberList = members
        payload.update({'subscribees': memberList})
        result = self.get_result(self.download_html(url, payload))
        # print("INFO: members added")
        self.activate_banlist()
        return result

    def remove_members(self, members):
        # subscribers will be removed to mailings
        # this is down by "downloading" HTML site of members and put new addresses in the GET-request as fields=
        # payload
        # downloaded website will not be used it is just used for GET-request with payload
        # more than one mail address can be delivered as array or separated by \n
        url = self.url + '/members/remove'
        payload = self.password.copy()
        if isinstance(members, list):
            memberList = ""
            for member in members:
                memberList += member + '\n'
        else:
            memberList = members
        payload.update({'unsubscribees': memberList})
        result = self.get_result(self.download_html(url, payload))
        # print("INFO: members removed")
        return result


