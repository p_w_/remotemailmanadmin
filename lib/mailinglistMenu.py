import copy

import PySimpleGUI as sg

from lib import logfile_handler, bundledFunctions


class MailinglistMenu:
    layout = []
    window = 0
    jsonData = ''
    log = ''
    jsonDataOriginal = ''

    def __init__(self, jsonData, title='RemoteMailManAdmin', log=logfile_handler.Log(enabled=False)):
        dropDownItems, null = bundledFunctions.generate_items_for_listbox(jsonData)
        self.log = log
        self.jsonData = copy.deepcopy(jsonData)
        self.jsonDataOriginal = jsonData
        self.generate_layout(dropDownItems)
        self.window = sg.Window(title, self.layout, default_element_size=(40, 1), grab_anywhere=False)

    def generate_layout(self, dropDownItems):
        self.layout = [
            [sg.Text('MailingLists', size=(22, 1)),
             sg.Text('Password', size=(20, 1)),
             sg.Text('URL', size=(40, 1))
             ],
            [sg.DropDown(dropDownItems, size=(20, 1), tooltip='Mailinglist', key='DROPDOWN', enable_events=True),
                sg.InputText('', size=(20, 1), tooltip='Password', key='PASSWORD'),
                sg.InputText('', size=(40, 1), tooltip='URL', key='URL')
             ],
            [sg.Text('Values will only be changed in json file not in mailinglist online!')],
            [sg.Button('Clear Boxes', key='CLEAR'),
             sg.Button('Add/Change', key='ADD-CHANGE'),
             sg.Button('Remove', key='REMOVE'),
             sg.Submit('Save', key='SAVE'), sg.Cancel('Quit', key='QUIT')]
        ]

    def clear_boxes(self):
        self.window['DROPDOWN'].update('')
        self.window['PASSWORD'].update('')
        self.window['URL'].update('')

    def start(self):
        closeWindow = 0
        # window will be shown as long as closed
        while True:
            event, values = self.window.read()

            if event == sg.WINDOW_CLOSED or event == 'QUIT':
                closeWindow = 1
                self.jsonData = self.jsonDataOriginal
                break

            elif event == 'DROPDOWN':
                if values['DROPDOWN'] in self.jsonData:
                    self.window['PASSWORD'].update(self.jsonData[values['DROPDOWN']]['password'])
                    self.window['URL'].update(self.jsonData[values['DROPDOWN']]['website'])

            elif event == 'CLEAR':
                self.clear_boxes()

            elif event == 'ADD-CHANGE':
                if values['DROPDOWN'] in self.jsonData:
                    self.jsonData[values['DROPDOWN']]['password'] = values['PASSWORD']
                    self.jsonData[values['DROPDOWN']]['website'] = values['URL']
                elif values['DROPDOWN']:
                    tempDict = {
                        'name': values['DROPDOWN'],
                        'website': values['URL'],
                        'password': values['PASSWORD']
                    }
                    self.jsonData.update({values['DROPDOWN']: tempDict})
                    self.window['DROPDOWN'].Values.append(values['DROPDOWN'])
                    self.window['DROPDOWN'].update(values['DROPDOWN'], values=self.window['DROPDOWN'].Values)

            elif event == 'REMOVE':
                if values['DROPDOWN'] in self.jsonData:
                    del self.jsonData[values['DROPDOWN']]
                    self.window['DROPDOWN'].Values.remove(values['DROPDOWN'])
                    self.window['DROPDOWN'].update(values['DROPDOWN'], values=self.window['DROPDOWN'].Values)
                    self.clear_boxes()

            elif event == 'SAVE':
                closeWindow = 1
                break

        return bool(closeWindow)

    def close(self):
        self.window.close()
        return self.jsonData
