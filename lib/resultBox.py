import PySimpleGUI as sg


class ResultBox:
    layout = []
    window = 0

    def __init__(self, title='Result', infoText=''):
        self.generate_layout(infoText)
        self.window = sg.Window(title, self.layout, default_element_size=(40, 40), grab_anywhere=False)

    def generate_layout(self, infotext):
        self.layout = [
            [sg.Multiline(infotext, key='PASSWORD', disabled=True, size=(30, 30))],
            [sg.Submit('OK', key='SUBMIT')]
        ]

    def start(self):
        while True:
            event, values = self.window.read()

            if event == sg.WINDOW_CLOSED or event == 'QUIT':
                self.window.close()
                return True

            if event == 'SUBMIT':
                self.window.close()
                return True
