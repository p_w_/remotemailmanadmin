def generate_items_for_listbox(jsonData):
    # This function returns all names from JSON/DICT
    # it is used while creating layout to fill listbox
    items = []
    indexes = []
    count = 0
    for value in jsonData:
        # print(jsonData[value]['website'])
        items.append(value)
        indexes.append(count)
        count = count+1
    return items, indexes


def get_positions_in_list(stringarray, jsonData):
    # This functions reads strings and checks position in JSON/DICT
    positions = []
    for sitem in stringarray:
        try:
            positions.append(list(jsonData).index(sitem))
        except:
            print('WARN: ' + 'MailingList ' + sitem +
                  ' could not be found!')
    return positions


def remove_redundant_data(subscriber_list):
    c = []
    for i in subscriber_list:
        if i not in c:
            c.append(i)
    return c
