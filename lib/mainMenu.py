import PySimpleGUI as sg

from . import mailmanMemberMenu as mamamembermenu, \
    passwordBox, \
    passwordreader, \
    mailman, \
    logfile_handler, mailinglistMenu, bundledFunctions


class MainMenu:
    layout = []
    window = 0
    jsonFile = ''
    jsonData = ''
    subOutputCSV = ''
    defaultMailingLists = ''
    buttonSize = (32, 3)
    log = ''

    def __init__(self, jsonFile, defaultMailingLists, outputCSV, title='RemoteMailManAdmin', log=logfile_handler.Log(enabled=False)):
        self.jsonFile = jsonFile
        self.log = log
        self.get_json(jsonFile)
        self.subOutputCSV = outputCSV
        self.defaultMailingLists = defaultMailingLists
        self.generate_layout()
        self.window = sg.Window(title, self.layout, default_element_size=(40, 1), grab_anywhere=False)

    def generate_layout(self):
        self.layout = [
            [sg.Button('Reset Password', key='RESET-PASSWORD', size=self.buttonSize)],
            [sg.Button('Get all Subscribers', key='GET-SUBS', size=self.buttonSize)],
            [sg.Button('Manage Subscribers', key='MANAGE-SUBS', size=self.buttonSize)],
            [sg.Button('Manage Mailinglists', key='MANAGE-LISTS', size=self.buttonSize)],
            [sg.ProgressBar(10, size=(23, 10), bar_color=('Green', 'White'), key='PROGRESSBAR')],
            [sg.Cancel('Quit', key='QUIT', size=self.buttonSize)]
        ]

    def get_json(self, jsonFile):
        title = 'Enter Password'
        if passwordreader.check_if_file_is_encrypted(jsonFile):
            title = 'Enter new Password'
        pwBox = passwordBox.PasswordBox(title=title)
        password = pwBox.start()

        if password:
            try:
                self.jsonData = passwordreader.get_json(jsonFile, bytes(password, 'utf-8'))
            except:
                sg.popup_error('Error: wrong password!')
                quit()
        else:
            quit()

    def save_file(self, jsonData):
        pwBox = passwordBox.PasswordBox(title='Enter old or new Password to save changes')
        password = pwBox.start()

        if password:
            try:
                passwordreader.save_changes(self.jsonFile, jsonData, bytes(password, 'utf-8'))
                return True
            except Exception as e:
                self.log.add('ERR', e)
                sg.popup_error('Data could not be written...')
                return False
        else:
            sg.popup_error('Changes dropped!')
            return False

    def get_all_subscribers(self):
        self.log.add('INFO', 'Generate new ' + self.subOutputCSV)
        progressbar = self.window['PROGRESSBAR']
        subscribers = []
        subscribers_in_lists = []
        header = 'subscriber'
        counter = 0
        progressbar.update(counter, max=len(self.jsonData))
        for maillist in self.jsonData:
            header += ","+maillist
            # print(maillist)
            mm = mailman.Mailman(self.jsonData[maillist]['website'], self.jsonData[maillist]['password'])
            subs = mm.get_users()
            subscribers += subs
            subscribers_in_lists.append(subs)
            counter = counter+1
            progressbar.update(counter)
        subscribers = bundledFunctions.remove_redundant_data(subscribers)
        # print(header)
        file = open(self.subOutputCSV, 'w')
        file.write(header+'\n')
        for subscriber in subscribers:
            subscriber_line = subscriber
            # print(subscriber)
            for subscribers_in_list in subscribers_in_lists:
                # print(len(subscribers_in_list))
                if subscriber in subscribers_in_list:
                    subscriber_line += ',x'
                else:
                    subscriber_line += ','
            # print(subscriber_line)
            file = open(self.subOutputCSV, 'a')
            file.write(subscriber_line+'\n')
        sg.popup_ok('Generated new ' + self.subOutputCSV)
        progressbar.update(0)

    def run(self):
        while True:
            event, values = self.window.read()

            if event == sg.WINDOW_CLOSED or event == 'QUIT':
                self.window.close()
                quit()

            if event == 'RESET-PASSWORD':
                self.window.disappear()
                pwBox = passwordBox.PasswordBox(title='Enter old Password')
                try:
                    oldpw = bytes(pwBox.start(), 'utf-8')
                    if oldpw:
                        pwBox = passwordBox.PasswordBox(title='Enter new Password')
                        newpw = bytes(pwBox.start(), 'utf-8')
                        if newpw:
                            result = passwordreader.reset_password_encryption(self.jsonFile, oldpw, newpw)
                            if 'ERR:' in result:
                                sg.popup_error(result, title='ERROR')
                                self.log.add('ERR', 'Password reset failed.')
                            else:
                                sg.popup(result, title='SUCCESS')
                                self.log.add('INFO', 'Password reset succeeded.')
                        else:
                            sg.popup_error('No Password entered', title='ERROR')
                            self.log.add('ERR', 'No Password entered')
                    else:
                        sg.popup_error('No Password entered', title='ERROR')
                        self.log.add('ERR', 'No Password entered')
                except:
                    pass
                self.window.reappear()

            elif event == 'GET-SUBS':
                self.get_all_subscribers()

            elif event == 'MANAGE-SUBS':
                mamaMenu = mamamembermenu.MailmanMenu(self.jsonData, self.defaultMailingLists, log=self.log)
                self.window.disappear()
                if mamaMenu.start():  # returns true if Quit Button is clicked
                    mamaMenu.close()
                self.window.reappear()

            elif event == 'MANAGE-LISTS':
                mlMenu = mailinglistMenu.MailinglistMenu(self.jsonData, log=self.log)
                self.window.disappear()
                if mlMenu.start():
                    newJsonData = mlMenu.close()
                    if newJsonData != self.jsonData:
                        if self.save_file(newJsonData):
                            self.jsonData = newJsonData
                            self.log.add('INFO', 'Updated '+self.jsonFile)
                    else:
                        for mlist in newJsonData:
                            if newJsonData[mlist] != self.jsonData[mlist]:
                                if self.save_file(newJsonData):
                                    self.jsonData = newJsonData
                                    self.log.add('INFO', 'Updated ' + self.jsonFile)
                self.window.reappear()
