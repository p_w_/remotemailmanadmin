import PySimpleGUI as sg

from . import mailman, logfile_handler, bundledFunctions, resultBox


class MailmanMenu:
    jsonData = 0
    layout = []
    window = 0
    listbox = 0
    progressbar = 0
    defaultsButton = 0
    defaultIndexes = []
    allIndexes = []
    log = ''

    def __init__(self, jsonData, defaultMailingLists, log=logfile_handler.Log(enabled=False)):
        listboxItems, self.allIndexes = bundledFunctions.generate_items_for_listbox(jsonData)
        self.jsonData = jsonData
        self.log = log
        self.generate_layout(listboxItems)
        self.window = sg.Window('RemoteMailmanAdmin', self.layout, default_element_size=(40, 1), grab_anywhere=False)
        self.listbox = self.window['LISTBOX-MAILINGLISTS']
        self.progressbar = self.window['PROGRESSBAR']
        self.defaultsButton = self.window['SELECT-DEFAULTS']
        self.defaultIndexes = bundledFunctions.get_positions_in_list(defaultMailingLists, jsonData)

    def generate_layout(self, listboxItems):
        self.layout = [
            [sg.Text('Enter EMail-address:')],
            [sg.Multiline(size=(32, 5), key='EMAILS', tooltip='Put every email-address in a separate line.')],
            [sg.Frame('Select', [[
                    sg.Button('All', key='SELECT-ALL'),
                    sg.Button('Default', key='SELECT-DEFAULTS'),
                    sg.Button('None', key='SELECT-NONE')
                ]])],
            [sg.Frame('Options', [[
                    sg.Radio('Add', 'Radio1', default=True, key='RADIO-ADD'),
                    sg.Radio('Remove', 'Radio1', key='RADIO-REMOVE')
                ]])],
            [  # sg.Listbox(values=generate_items_for_listbox(self.jsonData),
             sg.Listbox(values=listboxItems,
                        size=(32, 10),
                        select_mode='multiple',
                        key='LISTBOX-MAILINGLISTS'),

             ],
            [sg.ProgressBar(10, size=(21, 10), bar_color=('Green', 'White'), key='PROGRESSBAR')],  # 10 is a placeholder value and will be overwritten
            [sg.Submit('Submit', key='SUBMIT'), sg.Cancel('Quit', key='QUIT')]
        ]

    def disable_layout(self):
        self.window['LISTBOX-MAILINGLISTS'].update(disabled=True)
        self.window['SUBMIT'].update(disabled=True)
        self.window['QUIT'].update(disabled=True)
        self.window['RADIO-ADD'].update(disabled=True)
        self.window['RADIO-REMOVE'].update(disabled=True)
        self.window['EMAILS'].update(disabled=True)
        self.window['SELECT-DEFAULTS'].update(disabled=True)

    def enable_layout(self):
        self.window['LISTBOX-MAILINGLISTS'].update(disabled=False)
        self.window['SUBMIT'].update(disabled=False)
        self.window['QUIT'].update(disabled=False)
        self.window['RADIO-ADD'].update(disabled=False)
        self.window['RADIO-REMOVE'].update(disabled=False)
        self.window['EMAILS'].update(disabled=False)
        self.window['SELECT-DEFAULTS'].update(disabled=False)

    def submit_button(self, values):
        if values['RADIO-ADD']:
            result = 'add subscribers' + ' \n'
        elif values['RADIO-REMOVE']:
            result = 'remove subscribers' + ' \n'
        else:
            result = '' + ' \n'

        if values['EMAILS']:
            if values['LISTBOX-MAILINGLISTS']:
                self.disable_layout()
                # print(values)
                self.progressbar.update(0, max=len(values['LISTBOX-MAILINGLISTS']))
                counter = 0
                for maillist in values['LISTBOX-MAILINGLISTS']:
                    result += ' --- ' + maillist + ' --- \n'
                    # print('INFO: mailinglist = ' + maillist)
                    mm = mailman.Mailman(self.jsonData[maillist]['website'], self.jsonData[maillist]['password'])
                    # print(mm.get_users())
                    if values['RADIO-ADD']:
                        result += mm.add_members(values['EMAILS'])
                    elif values['RADIO-REMOVE']:
                        result += mm.remove_members(values['EMAILS'])
                    counter = counter+1
                    self.progressbar.update(counter)
                self.log.add('INFO', result)
                # sg.popup_ok(result)
                rb = resultBox.ResultBox(infoText=result)
                rb.start()
                self.enable_layout()
                self.progressbar.update(0)
            else:
                sg.popup_error('Missing selected mailinglists!')
        else:
            sg.popup_error('Missing Email addresses!')

    def start(self):
        closeWindow = 0
        # window will be shown as long as closed
        while True:
            event, values = self.window.read()

            if event == sg.WINDOW_CLOSED or event == 'QUIT':
                closeWindow = 1
                break

            elif event == 'SUBMIT':
                self.submit_button(values)

            elif event == 'SELECT-NONE':
                self.listbox.update(set_to_index=[])

            elif event == 'SELECT-ALL':
                self.listbox.update(set_to_index=self.allIndexes)

            elif event == 'SELECT-DEFAULTS':
                self.listbox.update(set_to_index=self.defaultIndexes)

        return bool(closeWindow)

    def close(self):
        self.window.close()
