if [ ! -f ./main.py ]
then
    echo "Please change directory!"
    exit 1
fi

if [ ! -d ./venv/ ]
then
    python -m venv venv
    source venv/bin/activate
    pip install -r requirements.txt
else
    source venv/bin/activate
fi

python ./main.py
deactivate
