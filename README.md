# RemoteMailManAdmin
**RemoteMailManAdmin** is a python program used to administrate a lot of different mailman sites without an open api.

I created this tool for a solidarity agriculture association with 25 maillinglists. The Members are added manually, public subscribers are not allowed. Originally created to capture all subscribers, I have extended it to add and remove these.
First it was written in bash. I challenged myself to get used to python, so it is basically my first python program.

The connection information stored in list.json will be hased with SHA256 after the first start.

## Requirements local
- **python3**
- a GUI

## Requirements target
- mailman (tested with version 2.1.35)

## <span style="color:red;">Caution!!</span>
The program heavily depends on the layout of the page, because the HTML code is read out. If the page layout changes, the program may not work anymore!

## How to start
- Download
- edit list.json (can also be edited later in the porgam "Manage Mailinglists")
  - Linux: ./rmamaa.sh
  - Windows: ./rmamaa.bat
- You have to enter a password to open list.json
  - <i>at first start password will only be used to encrypt list.json, this is the password for future use!</i>
- Now you can add new mailinglist to the connection file (list.json) with "Manage Mailinglists"
  - select an entry and change values or remove
  - write a new name in the first field and add password and URL to create a new entry
  - to write down changes to list.json press "Save"

## Config
There are some variables that can be changed through main.conf

| Variable | Effect                                                                                   | default         |
|----------|------------------------------------------------------------------------------------------|-----------------|
| connectionFile    | file where connection informations are stored                                            | list.json       |
| defaultMailingLists   | lists that will be selected when "Default" button in "Manage Subscribers" will be pressed | disabled        |
| allSubscribersOutputCSV | target file of "Get all Subscribers"                                                     | subscribers.csv |
| sessionLogging | logging for every session <br>- Added or removed Subscribers                             | enabled         |


